// swift-tools-version:5.3

import PackageDescription

let major = 3, minor = 2, patch = 2

let package = Package(
    name: "Olm",
    products: [
        .library(name: "libolm", targets: ["libolm"]),
        .library(name: "OLMKit", targets: ["OLMKit"])
    ],
    targets: [
        .target(
            name: "libolm",
            path: ".",
            sources: [
                "src",
                "lib/crypto-algorithms/aes.c",
                "lib/crypto-algorithms/sha256.c",
                "lib/curve25519-donna/curve25519-donna.c"
            ],
            cSettings: [
                .headerSearchPath("lib"),
// commenting out per https://github.com/matrix-org/olm/issues/51#issuecomment-804883045 suggestion
//                .unsafeFlags(["-g", "-O3", "-fPIC"]),
                .define("OLMLIB_VERSION_MAJOR", to: "\(major)"),
                .define("OLMLIB_VERSION_MINOR", to: "\(minor)"),
                .define("OLMLIB_VERSION_PATCH", to: "\(patch)")
            ]
        ),
        .target(
            name: "OLMKit",
            dependencies: ["libolm"],
            path: "xcode/OLMKit",
            exclude: ["Info.plist"],
            publicHeadersPath: ".",
            cSettings: [
                .headerSearchPath(".."),
// commenting out per https://github.com/matrix-org/olm/issues/51#issuecomment-804883045 suggestion
//                .unsafeFlags(["-g", "-O3"])
            ]
        ),
        .testTarget(
            name: "OLMKitTests",
            dependencies: ["OLMKit"],
            path: "xcode/OLMKitTests",
            exclude: ["Info.plist"],
            cSettings: [
                .headerSearchPath(".."),
                .unsafeFlags(["-g", "-O3"])
            ]
        )
    ],
    cLanguageStandard: .c99,
    cxxLanguageStandard: .cxx11
)
